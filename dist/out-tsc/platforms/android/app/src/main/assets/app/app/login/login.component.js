"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var user_master_1 = require("./../model/user/user-master");
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var core_2 = require("@angular/core");
var message_1 = require("../utilites/message");
var LoginComponent = /** @class */ (function () {
    function LoginComponent(route, router) {
        this.route = route;
        this.router = router;
        this.userDetails = new user_master_1.UserMaster();
    }
    LoginComponent.prototype.ngOnInit = function () {
    };
    LoginComponent.prototype.isValid = function () {
        if (this.userDetails.email == null) {
            this.alert('Enter your email');
            return false;
        }
        else if (this.userDetails.password == null) {
            this.alert('Enter your password');
            return false;
        }
        return true;
    };
    LoginComponent.prototype.btnLoginAction = function () {
        // console.log(Message.APP_NAME);
        // if (this.isValid()) {
        //   this.callUserLogin();
        // }
        this.callUserLogin();
    };
    LoginComponent.prototype.getEmailError = function () {
        var email = this.erroEmail.nativeElement;
        if (this.userDetails.email == null) {
            email.text = 'Helloo email';
        }
        else {
            email.text = '';
        }
    };
    LoginComponent.prototype.callUserLogin = function () {
        this.router.navigate(['/home']);
    };
    LoginComponent.prototype.alert = function (message) {
        return alert({
            title: message_1.Message.APP_NAME,
            okButtonText: message_1.Message.OK,
            message: message
        });
    };
    __decorate([
        core_2.ViewChild('password'),
        __metadata("design:type", core_2.ElementRef)
    ], LoginComponent.prototype, "passwordField", void 0);
    __decorate([
        core_2.ViewChild('email'),
        __metadata("design:type", core_2.ElementRef)
    ], LoginComponent.prototype, "emailField", void 0);
    __decorate([
        core_2.ViewChild('errormail'),
        __metadata("design:type", core_2.ElementRef)
    ], LoginComponent.prototype, "erroEmail", void 0);
    LoginComponent = __decorate([
        core_1.Component({
            selector: 'app-login',
            templateUrl: './login.component.html',
            styleUrls: ['./login.component.scss']
        }),
        __metadata("design:paramtypes", [router_1.ActivatedRoute,
            router_1.Router])
    ], LoginComponent);
    return LoginComponent;
}());
exports.LoginComponent = LoginComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vLi4vLi4vLi4vLi4vLi4vLi4vcGxhdGZvcm1zL2FuZHJvaWQvYXBwL3NyYy9tYWluL2Fzc2V0cy9hcHAvYXBwL2xvZ2luL2xvZ2luLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQUNBLDJEQUF5RDtBQUN6RCxzQ0FBa0Q7QUFDbEQsMENBQXlEO0FBQ3pELHNDQUFzRDtBQUV0RCwrQ0FBOEM7QUFPOUM7SUFTRSx3QkFBb0IsS0FBcUIsRUFDL0IsTUFBYztRQURKLFVBQUssR0FBTCxLQUFLLENBQWdCO1FBQy9CLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDcEIsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLHdCQUFVLEVBQUUsQ0FBQztJQUN2QyxDQUFDO0lBRUYsaUNBQVEsR0FBUjtJQUVBLENBQUM7SUFFRCxnQ0FBTyxHQUFQO1FBRUUsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssSUFBSSxJQUFJLEVBQUU7WUFDaEMsSUFBSSxDQUFDLEtBQUssQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1lBQy9CLE9BQU8sS0FBSyxDQUFDO1NBQ2hCO2FBQU0sSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsSUFBSSxJQUFJLEVBQUU7WUFDNUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO1lBQ2hDLE9BQU8sS0FBSyxDQUFDO1NBQ2hCO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDZCxDQUFDO0lBR0QsdUNBQWMsR0FBZDtRQUNFLGlDQUFpQztRQUNqQyx3QkFBd0I7UUFDeEIsMEJBQTBCO1FBQzFCLElBQUk7UUFDSixJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7SUFDdkIsQ0FBQztJQUVELHNDQUFhLEdBQWI7UUFDRSxJQUFNLEtBQUssR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQztRQUMzQyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxJQUFJLElBQUksRUFBRTtZQUNsQyxLQUFLLENBQUMsSUFBSSxHQUFHLGNBQWMsQ0FBQztTQUM3QjthQUFNO1lBQ0wsS0FBSyxDQUFDLElBQUksR0FBRyxFQUFFLENBQUM7U0FDakI7SUFDSCxDQUFDO0lBRUQsc0NBQWEsR0FBYjtRQUNFLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztJQUNsQyxDQUFDO0lBRUQsOEJBQUssR0FBTCxVQUFNLE9BQWU7UUFDbkIsT0FBTyxLQUFLLENBQUM7WUFDYixLQUFLLEVBQUUsaUJBQU8sQ0FBQyxRQUFRO1lBQ3ZCLFlBQVksRUFBRSxpQkFBTyxDQUFDLEVBQUU7WUFDeEIsT0FBTyxFQUFFLE9BQU87U0FDZixDQUFDLENBQUM7SUFDSCxDQUFDO0lBdERvQjtRQUF0QixnQkFBUyxDQUFDLFVBQVUsQ0FBQztrQ0FBZ0IsaUJBQVU7eURBQUM7SUFDN0I7UUFBbkIsZ0JBQVMsQ0FBQyxPQUFPLENBQUM7a0NBQWEsaUJBQVU7c0RBQUM7SUFDbkI7UUFBdkIsZ0JBQVMsQ0FBQyxXQUFXLENBQUM7a0NBQVksaUJBQVU7cURBQUM7SUFObkMsY0FBYztRQUwxQixnQkFBUyxDQUFDO1lBQ1QsUUFBUSxFQUFFLFdBQVc7WUFDckIsV0FBVyxFQUFFLHdCQUF3QjtZQUNyQyxTQUFTLEVBQUUsQ0FBQyx3QkFBd0IsQ0FBQztTQUN0QyxDQUFDO3lDQVUyQix1QkFBYztZQUN2QixlQUFNO09BVmIsY0FBYyxDQTREMUI7SUFBRCxxQkFBQztDQUFBLEFBNURELElBNERDO0FBNURZLHdDQUFjIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSG9tZUNvbXBvbmVudCB9IGZyb20gJy4vLi4vaG9tZS9ob21lLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBVc2VyTWFzdGVyIH0gZnJvbSAnLi8uLi9tb2RlbC91c2VyL3VzZXItbWFzdGVyJztcbmltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBSb3V0ZXIsIEFjdGl2YXRlZFJvdXRlIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7IFZpZXdDaGlsZCwgRWxlbWVudFJlZiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRm9ybUNvbnRyb2wgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQgeyBNZXNzYWdlIH0gZnJvbSAnLi4vdXRpbGl0ZXMvbWVzc2FnZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2FwcC1sb2dpbicsXG4gIHRlbXBsYXRlVXJsOiAnLi9sb2dpbi5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL2xvZ2luLmNvbXBvbmVudC5zY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgTG9naW5Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQgIHtcblxuICB1c2VyRGV0YWlsczogVXNlck1hc3RlcjtcblxuICBAVmlld0NoaWxkKCdwYXNzd29yZCcpIHBhc3N3b3JkRmllbGQ6IEVsZW1lbnRSZWY7XG4gIEBWaWV3Q2hpbGQoJ2VtYWlsJykgZW1haWxGaWVsZDogRWxlbWVudFJlZjtcbiAgQFZpZXdDaGlsZCgnZXJyb3JtYWlsJykgZXJyb0VtYWlsOiBFbGVtZW50UmVmO1xuXG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSByb3V0ZTogQWN0aXZhdGVkUm91dGUsXG4gICAgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlcikge1xuICAgICAgdGhpcy51c2VyRGV0YWlscyA9IG5ldyBVc2VyTWFzdGVyKCk7XG4gICB9XG5cbiAgbmdPbkluaXQoKSB7XG5cbiAgfVxuXG4gIGlzVmFsaWQoKTogYm9vbGVhbiB7XG5cbiAgICBpZiAodGhpcy51c2VyRGV0YWlscy5lbWFpbCA9PSBudWxsKSB7XG4gICAgICAgIHRoaXMuYWxlcnQoJ0VudGVyIHlvdXIgZW1haWwnKTtcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH0gZWxzZSBpZiAodGhpcy51c2VyRGV0YWlscy5wYXNzd29yZCA9PSBudWxsKSB7XG4gICAgICB0aGlzLmFsZXJ0KCdFbnRlciB5b3VyIHBhc3N3b3JkJyk7XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG4gICAgcmV0dXJuIHRydWU7XG4gIH1cblxuXG4gIGJ0bkxvZ2luQWN0aW9uKCkge1xuICAgIC8vIGNvbnNvbGUubG9nKE1lc3NhZ2UuQVBQX05BTUUpO1xuICAgIC8vIGlmICh0aGlzLmlzVmFsaWQoKSkge1xuICAgIC8vICAgdGhpcy5jYWxsVXNlckxvZ2luKCk7XG4gICAgLy8gfVxuICAgIHRoaXMuY2FsbFVzZXJMb2dpbigpO1xuICB9XG5cbiAgZ2V0RW1haWxFcnJvcigpIHtcbiAgICBjb25zdCBlbWFpbCA9IHRoaXMuZXJyb0VtYWlsLm5hdGl2ZUVsZW1lbnQ7XG4gICAgaWYgKHRoaXMudXNlckRldGFpbHMuZW1haWwgPT0gbnVsbCkge1xuICAgICAgZW1haWwudGV4dCA9ICdIZWxsb28gZW1haWwnO1xuICAgIH0gZWxzZSB7XG4gICAgICBlbWFpbC50ZXh0ID0gJyc7XG4gICAgfVxuICB9XG5cbiAgY2FsbFVzZXJMb2dpbigpIHtcbiAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbJy9ob21lJ10pO1xuICB9XG5cbiAgYWxlcnQobWVzc2FnZTogc3RyaW5nKSB7XG4gICAgcmV0dXJuIGFsZXJ0KHtcbiAgICB0aXRsZTogTWVzc2FnZS5BUFBfTkFNRSxcbiAgICBva0J1dHRvblRleHQ6IE1lc3NhZ2UuT0ssXG4gICAgbWVzc2FnZTogbWVzc2FnZVxuICAgIH0pO1xuICAgIH1cblxufVxuIl19