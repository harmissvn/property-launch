"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Message = /** @class */ (function () {
    function Message() {
    }
    Message.BOOK_SHELF_NONE = 'None';
    Message.BOOK_SHELF_FULL = 'Full';
    Message.APP_NAME = 'Property Launch';
    Message.OK = 'OK';
    return Message;
}());
exports.Message = Message;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWVzc2FnZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL3BsYXRmb3Jtcy9hbmRyb2lkL2FwcC9idWlsZC9pbnRlcm1lZGlhdGVzL21lcmdlZF9hc3NldHMvZGVidWcvbWVyZ2VEZWJ1Z0Fzc2V0cy9vdXQvYXBwL2FwcC91dGlsaXRlcy9tZXNzYWdlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQ0E7SUFBQTtJQU1FLENBQUM7SUFMZSx1QkFBZSxHQUFHLE1BQU0sQ0FBQztJQUN6Qix1QkFBZSxHQUFHLE1BQU0sQ0FBQztJQUN6QixnQkFBUSxHQUFHLGlCQUFpQixDQUFDO0lBQzdCLFVBQUUsR0FBRyxJQUFJLENBQUM7SUFFMUIsY0FBQztDQUFBLEFBTkgsSUFNRztBQU5VLDBCQUFPIiwic291cmNlc0NvbnRlbnQiOlsiXG5leHBvcnQgY2xhc3MgTWVzc2FnZSB7XG4gICAgcHVibGljIHN0YXRpYyBCT09LX1NIRUxGX05PTkUgPSAnTm9uZSc7XG4gICAgcHVibGljIHN0YXRpYyBCT09LX1NIRUxGX0ZVTEwgPSAnRnVsbCc7XG4gICAgcHVibGljIHN0YXRpYyBBUFBfTkFNRSA9ICdQcm9wZXJ0eSBMYXVuY2gnO1xuICAgIHB1YmxpYyBzdGF0aWMgT0sgPSAnT0snO1xuXG4gIH1cbiJdfQ==