"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ApiUrls = /** @class */ (function () {
    function ApiUrls() {
    }
    ApiUrls.MAIN_URL = 'https://jobsite.mn:8012/api/';
    ApiUrls.LOGIN_URL = ApiUrls.MAIN_URL + 'TokenAuth/AuthenticateWithDeviceToken';
    return ApiUrls;
}());
exports.ApiUrls = ApiUrls;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBpTmFtZXMuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi8uLi8uLi8uLi8uLi8uLi8uLi8uLi8uLi8uLi9wbGF0Zm9ybXMvYW5kcm9pZC9hcHAvYnVpbGQvaW50ZXJtZWRpYXRlcy9tZXJnZWRfYXNzZXRzL2RlYnVnL21lcmdlRGVidWdBc3NldHMvb3V0L2FwcC9hcHAvdXRpbGl0ZXMvYXBpTmFtZXMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFFQTtJQUFBO0lBTUUsQ0FBQztJQUplLGdCQUFRLEdBQUcsOEJBQThCLENBQUM7SUFFMUMsaUJBQVMsR0FBRyxPQUFPLENBQUMsUUFBUSxHQUFHLHVDQUF1QyxDQUFDO0lBRXZGLGNBQUM7Q0FBQSxBQU5ILElBTUc7QUFOVSwwQkFBTyIsInNvdXJjZXNDb250ZW50IjpbIlxuXG5leHBvcnQgY2xhc3MgQXBpVXJscyB7XG5cbiAgICBwdWJsaWMgc3RhdGljIE1BSU5fVVJMID0gJ2h0dHBzOi8vam9ic2l0ZS5tbjo4MDEyL2FwaS8nO1xuXG4gICAgcHVibGljIHN0YXRpYyBMT0dJTl9VUkwgPSBBcGlVcmxzLk1BSU5fVVJMICsgJ1Rva2VuQXV0aC9BdXRoZW50aWNhdGVXaXRoRGV2aWNlVG9rZW4nO1xuXG4gIH1cblxuXG4iXX0=