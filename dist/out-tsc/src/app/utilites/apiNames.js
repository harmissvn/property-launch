"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ApiUrls = /** @class */ (function () {
    function ApiUrls() {
    }
    ApiUrls.MAIN_URL = 'https://jobsite.mn:8012/api/';
    ApiUrls.LOGIN_URL = ApiUrls.MAIN_URL + 'TokenAuth/AuthenticateWithDeviceToken';
    return ApiUrls;
}());
exports.ApiUrls = ApiUrls;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBpTmFtZXMuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi9zcmMvYXBwL3V0aWxpdGVzL2FwaU5hbWVzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBRUE7SUFBQTtJQU1FLENBQUM7SUFKZSxnQkFBUSxHQUFHLDhCQUE4QixDQUFDO0lBRTFDLGlCQUFTLEdBQUcsT0FBTyxDQUFDLFFBQVEsR0FBRyx1Q0FBdUMsQ0FBQztJQUV2RixjQUFDO0NBQUEsQUFOSCxJQU1HO0FBTlUsMEJBQU8iLCJzb3VyY2VzQ29udGVudCI6WyJcblxuZXhwb3J0IGNsYXNzIEFwaVVybHMge1xuXG4gICAgcHVibGljIHN0YXRpYyBNQUlOX1VSTCA9ICdodHRwczovL2pvYnNpdGUubW46ODAxMi9hcGkvJztcblxuICAgIHB1YmxpYyBzdGF0aWMgTE9HSU5fVVJMID0gQXBpVXJscy5NQUlOX1VSTCArICdUb2tlbkF1dGgvQXV0aGVudGljYXRlV2l0aERldmljZVRva2VuJztcblxuICB9XG5cblxuIl19