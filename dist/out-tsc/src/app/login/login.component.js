"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var user_master_1 = require("./../model/user/user-master");
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var core_2 = require("@angular/core");
var message_1 = require("../utilites/message");
var LoginComponent = /** @class */ (function () {
    function LoginComponent(route, router) {
        this.route = route;
        this.router = router;
        this.userDetails = new user_master_1.UserMaster();
    }
    LoginComponent.prototype.ngOnInit = function () {
    };
    LoginComponent.prototype.isValid = function () {
        if (this.userDetails.email == null) {
            this.alert('Enter your email');
            return false;
        }
        else if (this.userDetails.password == null) {
            this.alert('Enter your password');
            return false;
        }
        return true;
    };
    LoginComponent.prototype.btnLoginAction = function () {
        // console.log(Message.APP_NAME);
        // if (this.isValid()) {
        //   this.callUserLogin();
        // }
        this.callUserLogin();
    };
    LoginComponent.prototype.getEmailError = function () {
        var email = this.erroEmail.nativeElement;
        if (this.userDetails.email == null) {
            email.text = 'Helloo email';
        }
        else {
            email.text = '';
        }
    };
    LoginComponent.prototype.callUserLogin = function () {
        this.router.navigate(['/home']);
    };
    LoginComponent.prototype.alert = function (message) {
        return alert({
            title: message_1.Message.APP_NAME,
            okButtonText: message_1.Message.OK,
            message: message
        });
    };
    __decorate([
        core_2.ViewChild('password'),
        __metadata("design:type", core_2.ElementRef)
    ], LoginComponent.prototype, "passwordField", void 0);
    __decorate([
        core_2.ViewChild('email'),
        __metadata("design:type", core_2.ElementRef)
    ], LoginComponent.prototype, "emailField", void 0);
    __decorate([
        core_2.ViewChild('errormail'),
        __metadata("design:type", core_2.ElementRef)
    ], LoginComponent.prototype, "erroEmail", void 0);
    LoginComponent = __decorate([
        core_1.Component({
            selector: 'app-login',
            templateUrl: './login.component.html',
            styleUrls: ['./login.component.scss']
        }),
        __metadata("design:paramtypes", [router_1.ActivatedRoute,
            router_1.Router])
    ], LoginComponent);
    return LoginComponent;
}());
exports.LoginComponent = LoginComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vc3JjL2FwcC9sb2dpbi9sb2dpbi5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7QUFDQSwyREFBeUQ7QUFDekQsc0NBQWtEO0FBQ2xELDBDQUF5RDtBQUN6RCxzQ0FBc0Q7QUFFdEQsK0NBQThDO0FBTzlDO0lBU0Usd0JBQW9CLEtBQXFCLEVBQy9CLE1BQWM7UUFESixVQUFLLEdBQUwsS0FBSyxDQUFnQjtRQUMvQixXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQ3BCLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSx3QkFBVSxFQUFFLENBQUM7SUFDdkMsQ0FBQztJQUVGLGlDQUFRLEdBQVI7SUFFQSxDQUFDO0lBRUQsZ0NBQU8sR0FBUDtRQUVFLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLElBQUksSUFBSSxFQUFFO1lBQ2hDLElBQUksQ0FBQyxLQUFLLENBQUMsa0JBQWtCLENBQUMsQ0FBQztZQUMvQixPQUFPLEtBQUssQ0FBQztTQUNoQjthQUFNLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLElBQUksSUFBSSxFQUFFO1lBQzVDLElBQUksQ0FBQyxLQUFLLENBQUMscUJBQXFCLENBQUMsQ0FBQztZQUNoQyxPQUFPLEtBQUssQ0FBQztTQUNoQjtRQUNELE9BQU8sSUFBSSxDQUFDO0lBQ2QsQ0FBQztJQUdELHVDQUFjLEdBQWQ7UUFDRSxpQ0FBaUM7UUFDakMsd0JBQXdCO1FBQ3hCLDBCQUEwQjtRQUMxQixJQUFJO1FBQ0osSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO0lBQ3ZCLENBQUM7SUFFRCxzQ0FBYSxHQUFiO1FBQ0UsSUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxhQUFhLENBQUM7UUFDM0MsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssSUFBSSxJQUFJLEVBQUU7WUFDbEMsS0FBSyxDQUFDLElBQUksR0FBRyxjQUFjLENBQUM7U0FDN0I7YUFBTTtZQUNMLEtBQUssQ0FBQyxJQUFJLEdBQUcsRUFBRSxDQUFDO1NBQ2pCO0lBQ0gsQ0FBQztJQUVELHNDQUFhLEdBQWI7UUFDRSxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7SUFDbEMsQ0FBQztJQUVELDhCQUFLLEdBQUwsVUFBTSxPQUFlO1FBQ25CLE9BQU8sS0FBSyxDQUFDO1lBQ2IsS0FBSyxFQUFFLGlCQUFPLENBQUMsUUFBUTtZQUN2QixZQUFZLEVBQUUsaUJBQU8sQ0FBQyxFQUFFO1lBQ3hCLE9BQU8sRUFBRSxPQUFPO1NBQ2YsQ0FBQyxDQUFDO0lBQ0gsQ0FBQztJQXREb0I7UUFBdEIsZ0JBQVMsQ0FBQyxVQUFVLENBQUM7a0NBQWdCLGlCQUFVO3lEQUFDO0lBQzdCO1FBQW5CLGdCQUFTLENBQUMsT0FBTyxDQUFDO2tDQUFhLGlCQUFVO3NEQUFDO0lBQ25CO1FBQXZCLGdCQUFTLENBQUMsV0FBVyxDQUFDO2tDQUFZLGlCQUFVO3FEQUFDO0lBTm5DLGNBQWM7UUFMMUIsZ0JBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxXQUFXO1lBQ3JCLFdBQVcsRUFBRSx3QkFBd0I7WUFDckMsU0FBUyxFQUFFLENBQUMsd0JBQXdCLENBQUM7U0FDdEMsQ0FBQzt5Q0FVMkIsdUJBQWM7WUFDdkIsZUFBTTtPQVZiLGNBQWMsQ0E0RDFCO0lBQUQscUJBQUM7Q0FBQSxBQTVERCxJQTREQztBQTVEWSx3Q0FBYyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEhvbWVDb21wb25lbnQgfSBmcm9tICcuLy4uL2hvbWUvaG9tZS5jb21wb25lbnQnO1xuaW1wb3J0IHsgVXNlck1hc3RlciB9IGZyb20gJy4vLi4vbW9kZWwvdXNlci91c2VyLW1hc3Rlcic7XG5pbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgUm91dGVyLCBBY3RpdmF0ZWRSb3V0ZSB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XG5pbXBvcnQgeyBWaWV3Q2hpbGQsIEVsZW1lbnRSZWYgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEZvcm1Db250cm9sIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuaW1wb3J0IHsgTWVzc2FnZSB9IGZyb20gJy4uL3V0aWxpdGVzL21lc3NhZ2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdhcHAtbG9naW4nLFxuICB0ZW1wbGF0ZVVybDogJy4vbG9naW4uY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9sb2dpbi5jb21wb25lbnQuc2NzcyddXG59KVxuZXhwb3J0IGNsYXNzIExvZ2luQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0ICB7XG5cbiAgdXNlckRldGFpbHM6IFVzZXJNYXN0ZXI7XG5cbiAgQFZpZXdDaGlsZCgncGFzc3dvcmQnKSBwYXNzd29yZEZpZWxkOiBFbGVtZW50UmVmO1xuICBAVmlld0NoaWxkKCdlbWFpbCcpIGVtYWlsRmllbGQ6IEVsZW1lbnRSZWY7XG4gIEBWaWV3Q2hpbGQoJ2Vycm9ybWFpbCcpIGVycm9FbWFpbDogRWxlbWVudFJlZjtcblxuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgcm91dGU6IEFjdGl2YXRlZFJvdXRlLFxuICAgIHByaXZhdGUgcm91dGVyOiBSb3V0ZXIpIHtcbiAgICAgIHRoaXMudXNlckRldGFpbHMgPSBuZXcgVXNlck1hc3RlcigpO1xuICAgfVxuXG4gIG5nT25Jbml0KCkge1xuXG4gIH1cblxuICBpc1ZhbGlkKCk6IGJvb2xlYW4ge1xuXG4gICAgaWYgKHRoaXMudXNlckRldGFpbHMuZW1haWwgPT0gbnVsbCkge1xuICAgICAgICB0aGlzLmFsZXJ0KCdFbnRlciB5b3VyIGVtYWlsJyk7XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9IGVsc2UgaWYgKHRoaXMudXNlckRldGFpbHMucGFzc3dvcmQgPT0gbnVsbCkge1xuICAgICAgdGhpcy5hbGVydCgnRW50ZXIgeW91ciBwYXNzd29yZCcpO1xuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuICAgIHJldHVybiB0cnVlO1xuICB9XG5cblxuICBidG5Mb2dpbkFjdGlvbigpIHtcbiAgICAvLyBjb25zb2xlLmxvZyhNZXNzYWdlLkFQUF9OQU1FKTtcbiAgICAvLyBpZiAodGhpcy5pc1ZhbGlkKCkpIHtcbiAgICAvLyAgIHRoaXMuY2FsbFVzZXJMb2dpbigpO1xuICAgIC8vIH1cbiAgICB0aGlzLmNhbGxVc2VyTG9naW4oKTtcbiAgfVxuXG4gIGdldEVtYWlsRXJyb3IoKSB7XG4gICAgY29uc3QgZW1haWwgPSB0aGlzLmVycm9FbWFpbC5uYXRpdmVFbGVtZW50O1xuICAgIGlmICh0aGlzLnVzZXJEZXRhaWxzLmVtYWlsID09IG51bGwpIHtcbiAgICAgIGVtYWlsLnRleHQgPSAnSGVsbG9vIGVtYWlsJztcbiAgICB9IGVsc2Uge1xuICAgICAgZW1haWwudGV4dCA9ICcnO1xuICAgIH1cbiAgfVxuXG4gIGNhbGxVc2VyTG9naW4oKSB7XG4gICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoWycvaG9tZSddKTtcbiAgfVxuXG4gIGFsZXJ0KG1lc3NhZ2U6IHN0cmluZykge1xuICAgIHJldHVybiBhbGVydCh7XG4gICAgdGl0bGU6IE1lc3NhZ2UuQVBQX05BTUUsXG4gICAgb2tCdXR0b25UZXh0OiBNZXNzYWdlLk9LLFxuICAgIG1lc3NhZ2U6IG1lc3NhZ2VcbiAgICB9KTtcbiAgICB9XG5cbn1cbiJdfQ==