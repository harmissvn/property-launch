"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Message = /** @class */ (function () {
    function Message() {
    }
    Message.BOOK_SHELF_NONE = 'None';
    Message.BOOK_SHELF_FULL = 'Full';
    Message.APP_NAME = 'Property Launch';
    Message.OK = 'OK';
    return Message;
}());
exports.Message = Message;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWVzc2FnZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uL3NyYy9hcHAvdXRpbGl0ZXMvbWVzc2FnZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUNBO0lBQUE7SUFNRSxDQUFDO0lBTGUsdUJBQWUsR0FBRyxNQUFNLENBQUM7SUFDekIsdUJBQWUsR0FBRyxNQUFNLENBQUM7SUFDekIsZ0JBQVEsR0FBRyxpQkFBaUIsQ0FBQztJQUM3QixVQUFFLEdBQUcsSUFBSSxDQUFDO0lBRTFCLGNBQUM7Q0FBQSxBQU5ILElBTUc7QUFOVSwwQkFBTyIsInNvdXJjZXNDb250ZW50IjpbIlxuZXhwb3J0IGNsYXNzIE1lc3NhZ2Uge1xuICAgIHB1YmxpYyBzdGF0aWMgQk9PS19TSEVMRl9OT05FID0gJ05vbmUnO1xuICAgIHB1YmxpYyBzdGF0aWMgQk9PS19TSEVMRl9GVUxMID0gJ0Z1bGwnO1xuICAgIHB1YmxpYyBzdGF0aWMgQVBQX05BTUUgPSAnUHJvcGVydHkgTGF1bmNoJztcbiAgICBwdWJsaWMgc3RhdGljIE9LID0gJ09LJztcblxuICB9XG4iXX0=