import { Component, OnInit } from '@angular/core';
import { UserMaster } from './../model/user/user-master';
import { Title } from '@angular/platform-browser';
import { Page } from 'tns-core-modules/ui/page';
import * as app from 'tns-core-modules/application';
import { RadSideDrawer } from 'nativescript-ui-sidedrawer';


@Component({
  selector: 'app-home',
  moduleId: module.id,
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {

  title = 'Bhavesh Sachala';

  userDetails: UserMaster;

  data = [];

  name: string;

  constructor(private page: Page) {

   }

   ngOnInit(): void {
}

  onDrawerButtonTap() {
    console.log('drawe tapped');
    const sideDrawer = <RadSideDrawer>app.getRootView();
    sideDrawer.showDrawer();
  }

}
