export class UserMaster {
    success: number;
    message: string;
    email: string;
    password: string;
    result: Result;
  }

  export class Result {
    userId: string;
    username: string;
    email: string;
    phone: string;
    password: string;
}