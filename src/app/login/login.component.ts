import { HomeComponent } from './../home/home.component';
import { UserMaster } from './../model/user/user-master';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ViewChild, ElementRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Message } from '../utilites/message';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit  {

  userDetails: UserMaster;

  @ViewChild('password') passwordField: ElementRef;
  @ViewChild('email') emailField: ElementRef;
  @ViewChild('errormail') erroEmail: ElementRef;


  constructor(private route: ActivatedRoute,
    private router: Router) {
      this.userDetails = new UserMaster();
   }

  ngOnInit() {

  }

  isValid(): boolean {

    if (this.userDetails.email == null) {
        this.alert('Enter your email');
        return false;
    } else if (this.userDetails.password == null) {
      this.alert('Enter your password');
        return false;
    }
    return true;
  }


  btnLoginAction() {
    // console.log(Message.APP_NAME);
    // if (this.isValid()) {
    //   this.callUserLogin();
    // }
    this.callUserLogin();
  }

  getEmailError() {
    const email = this.erroEmail.nativeElement;
    if (this.userDetails.email == null) {
      email.text = 'Helloo email';
    } else {
      email.text = '';
    }
  }

  callUserLogin() {
    this.router.navigate(['/home']);
  }

  alert(message: string) {
    return alert({
    title: Message.APP_NAME,
    okButtonText: Message.OK,
    message: message
    });
    }

}
