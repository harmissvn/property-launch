module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	var installedChunks = {
/******/ 		"bundle": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	var jsonpArray = global["webpackJsonp"] = global["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push(["./main.ts","vendor"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "../$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./app.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../node_modules/css-loader/lib/css-base.js")(false);
// imports
exports.i(__webpack_require__("../node_modules/css-loader/index.js?!../node_modules/nativescript-theme-core/css/core.light.css"), "");

// module
exports.push([module.i, "\n", ""]);

// exports
;
    if (false) {}


/***/ }),

/***/ "./app/app-routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/nativescript-angular/router/index.js");
/* harmony import */ var nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _app_routes__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./app/app.routes.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__["NativeScriptRouterModule"].forRoot(_app_routes__WEBPACK_IMPORTED_MODULE_2__["routes"])],
            exports: [nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__["NativeScriptRouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./app/app.component.css":
/***/ (function(module, exports) {

module.exports = "\n.fa {\n    font-family: \"FontAwesome\";\n}\n\n.var {\n    font-family: \"Roboto\", \"Roboto-Bold\";\n    font-weight: bold;\n    font-size: 17;\n}\n\n.fontColor {\n    color: white;  \n}\n\n.action-bar {\n    background-color: #191919;\n}\n\n.ab-icon {\n    color: white;\n}\n\n.page-content {\n    background-image: url(\"https://placem.at/places?random=11&h=1500&txt=0\");\n    background-size: cover;\n}\n\n.hr-light {\n    background-color: #aaaaaa;\n}\n\n.sidedrawer {\n    background-color: white;\n}\n\n.sidedrawer-header-text {\n    color: white;\n    font-size: 22;\n    font-weight: 800;\n    margin-top: 60;\n}\n\n.sidedrawer-header-footnote {\n    color: white;\n}\n\n.sidedrawer-list-item {\n    color: white;\n    height: 80;\n}\n.sidedrawer-list-item-active {\n    color: white;\n    height: 90;\n}\n\n.sidedrawer-item {\n    vertical-align:center;\n    color: #959292;\n    margin-top: 5;\n    margin-bottom: 5;\n}\n\n.menuTitle {\n     horizontal-align:center;\n     margin-top: 5;\n     height: 20;\n     margin-bottom: 5;\n     color: #959292;\n\n}\n.menuTitleActive {\n    horizontal-align: center;\n    margin-top: 5;\n    height: 20;\n    margin-bottom: 5;\n    color: white;\n\n}\n\n.sidedrawer-list-item-active {\n    background-color: #1877bb;\n}\n\n.menuLogo {\nhorizontal-align:center;\nwidth: 50;\nheight: 50;\nmargin-top: 5;\nmargin-bottom: 5;\n}\n\n.menuLogoSecond {\n    horizontal-align:center;\n    width: 34;\n    height: 34;\n    margin-top: 10;\n}\n\n.action-bar {\n    background-color: #1877bb;\n}\n\n.btnMenu {\n    width: 20;\n    height: 20;\n    margin-left: 12;\n    vertical-align:center;   \n}\n "

/***/ }),

/***/ "./app/app.component.html":
/***/ (function(module, exports) {

module.exports = "  <page-router-outlet></page-router-outlet>\n\n  <RadSideDrawer drawerTransition=\"PushTransition\" drawerContentSize=\"200\">\n\n    <GridLayout tkDrawerContent rows=\"auto, *\" class=\"sidedrawer sidedrawer-left\">\n        <ScrollView row=\"1\">\n            <StackLayout class=\"sidedrawer-content\">\n\n                    <GridLayout rows=\"auto, *\" class=\"sidedrawer-list-item-active\" title=\"Home\" tap=\"onNavigationItemTap\">\n                        <Image row=\"0\" src=\"~/app/images/ic_owner\" class=\"menuLogo\"></Image>\n                        <Label row=\"1\" text = \"Harmis\" class=\"fontColor var m-l-10 menuTitleActive\" ></Label>\n                    </GridLayout>\n                    \n                    <StackLayout class=\"hr-light\"></StackLayout>\n\n                <GridLayout rows=\"auto, *\" class=\"sidedrawer-list-item\"\n                    title=\"My Propertiess\" (tap) = \"onNavigationItemTap(this.title)\">\n                    <Image row=\"0\" src=\"~/app/images/ic_my_property.png\" class=\"menuLogoSecond\"></Image>\n                    <Label row=\"1\" text = \"My Propertiess\" class=\"fontColor var m-l-10 menuTitle\" ></Label>\n                </GridLayout>\n\n                <StackLayout class=\"hr-light\"></StackLayout>\n\n                <GridLayout rows=\"auto, *\" class=\"sidedrawer-list-item\"\n                    title=\"Home\" tap = \"onNavigationItemTap\">\n                    <Image row=\"0\" src=\"~/app/images/ic_home\" class=\"menuLogoSecond\"></Image>\n                    <Label row=\"1\" text=\"Add/Edit Property\" class=\"fontColor var m-l-10 menuTitle\" ></Label>\n                </GridLayout>\n\n                <StackLayout class=\"hr-light\"></StackLayout>\n\n                <GridLayout rows=\"auto, *\" class=\"sidedrawer-list-item\"\n                    title=\"Search\" tap=\"onNavigationItemTap\">\n                    <Image row=\"0\" src=\"~/app/images/ic_client_history\" class=\"menuLogoSecond\"></Image>\n                    <Label row=\"1\" text=\"Client Order History\" class=\"fontColor var m-l-10 menuTitle\" ></Label>\n                </GridLayout>\n\n                <StackLayout class=\"hr-light\"></StackLayout>\n\n                <GridLayout rows=\"auto, *\" class=\"sidedrawer-list-item\"\n                    title=\"Camera\" tap=\"onNavigationItemTap\">\n                    <Image row=\"0\" src=\"~/app/images/ic_example_work\" class=\"menuLogoSecond\"></Image>\n                    <Label row=\"1\" text=\"Example Work\" class=\"fontColor var m-l-10 menuTitle\" ></Label>\n                </GridLayout>\n\n                <StackLayout class=\"hr-light\"></StackLayout>\n\n                <GridLayout rows=\"auto, *\" class=\"sidedrawer-list-item\"\n                    title=\"Store\" tap=\"onNavigationItemTap\">\n                    <Image row=\"0\" src=\"~/app/images/ic_contact_admin\" class=\"menuLogoSecond\"></Image>\n                    <Label row=\"1\" text=\"Contact Admin\" class=\"fontColor var m-l-10 menuTitle\" ></Label>\n                </GridLayout>\n\n                <StackLayout class=\"hr-light\"></StackLayout>\n\n                <GridLayout rows=\"auto, *\" class=\"sidedrawer-list-item\"\n                    title=\"Store\" tap=\"onNavigationItemTap\">\n                    <Image row=\"0\" src=\"~/app/images/ic_calendar\" class=\"menuLogoSecond\"></Image>\n                    <Label row=\"1\" text=\"Calendar\" class=\"fontColor var m-l-10 menuTitle\" ></Label>\n                </GridLayout>\n\n                <StackLayout class=\"hr-light\"></StackLayout>\n\n                <GridLayout rows=\"auto, *\" class=\"sidedrawer-list-item\"\n                    title=\"Store\" tap=\"onNavigationItemTap\">\n                    <Image row=\"0\" src=\"~/app/images/ic_signout\" class=\"menuLogoSecond\"></Image>\n                    <Label row=\"1\" text=\"Signout\" class=\"fontColor var m-l-10 menuTitle\" ></Label>\n                </GridLayout>\n\n                <StackLayout class=\"hr-light\"></StackLayout>\n\n            </StackLayout>\n        </ScrollView>\n    </GridLayout>\n\n    <page-router-outlet tkMainContent class=\"page page-content\"></page-router-outlet>\n\n</RadSideDrawer>\n"

/***/ }),

/***/ "./app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
    }
    AppComponent.prototype.onNavigationItemTap = function (navigatioName) {
        console.log(navigatioName);
    };
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__("./app/app.component.html"),
            styles: [__webpack_require__("./app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var nativescript_angular_nativescript_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/nativescript-angular/nativescript.module.js");
/* harmony import */ var nativescript_angular_nativescript_module__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(nativescript_angular_nativescript_module__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./app/app.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("./app/home/home.component.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("./app/login/login.component.ts");
/* harmony import */ var nativescript_angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__("../node_modules/nativescript-angular/forms/index.js");
/* harmony import */ var nativescript_angular_forms__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(nativescript_angular_forms__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var nativescript_ui_sidedrawer_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__("../node_modules/nativescript-ui-sidedrawer/angular/side-drawer-directives.js");
/* harmony import */ var nativescript_ui_sidedrawer_angular__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(nativescript_ui_sidedrawer_angular__WEBPACK_IMPORTED_MODULE_7__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








// Uncomment and add to NgModule imports if you need to use two-way binding
// import { NativeScriptFormsModule } from 'nativescript-angular/forms';
// Uncomment and add to NgModule imports  if you need to use the HTTP wrapper
// import { NativeScriptHttpClientModule } from 'nativescript-angular/http-client';
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"],
                _home_home_component__WEBPACK_IMPORTED_MODULE_4__["HomeComponent"],
                _login_login_component__WEBPACK_IMPORTED_MODULE_5__["LoginComponent"],
            ],
            imports: [
                nativescript_angular_nativescript_module__WEBPACK_IMPORTED_MODULE_1__["NativeScriptModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_2__["AppRoutingModule"],
                nativescript_angular_forms__WEBPACK_IMPORTED_MODULE_6__["NativeScriptFormsModule"],
                nativescript_ui_sidedrawer_angular__WEBPACK_IMPORTED_MODULE_7__["NativeScriptUISideDrawerModule"],
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]],
            schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_0__["NO_ERRORS_SCHEMA"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./app/app.routes.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "routes", function() { return routes; });
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./app/login/login.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./app/home/home.component.ts");


var routes = [
    {
        path: '',
        redirectTo: '/login',
        pathMatch: 'full',
    },
    {
        path: 'login',
        component: _login_login_component__WEBPACK_IMPORTED_MODULE_0__["LoginComponent"],
    },
    {
        path: 'home',
        component: _home_home_component__WEBPACK_IMPORTED_MODULE_1__["HomeComponent"],
    },
];


/***/ }),

/***/ "./app/home/home.component.css":
/***/ (function(module, exports) {

module.exports = "\n.fa {\n    font-family: \"FontAwesome\";\n}\n\n.menu-icon {\n    font-size: 26; \n    color: white;\n    padding-left: 13;\n    vertical-align: center;\n} \n.action-bar {\n    background-color: #1877bb;\n}\n\n.page-content {\n    background-image: url(\"https://placem.at/places?random=11&h=1500&txt=0\");\n    background-size: cover;\n}\n\n.var {\n    font-family: \"Roboto\", \"Roboto-Bold\";\n    font-weight: bold;\n    font-size: 17;\n    color: white;  \n}"

/***/ }),

/***/ "./app/home/home.component.html":
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\n<ActionBar title=\"Home\" class=\" var action-bar\">\n\t<ActionItem>\n\t\t<StackLayout>\n\t\t\t<Image src=\"~/app/images/ic_menu\" width=\"20\" height=\"20\" (tap)=\"onDrawerButtonTap()\"></Image>\n\t\t</StackLayout>\n\t</ActionItem>\n</ActionBar>\n<StackLayout class=\"p-20\">\n    <Label text=\"Welcome to {{ title }}!\" class=\"h1 text-center\" textWrap=\"true\"></Label>\n</StackLayout>"

/***/ }),

/***/ "./app/home/home.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/tns-core-modules/ui/page/page.js");
/* harmony import */ var tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var tns_core_modules_application__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("../node_modules/tns-core-modules/application/application.js");
/* harmony import */ var tns_core_modules_application__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(tns_core_modules_application__WEBPACK_IMPORTED_MODULE_2__);
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var HomeComponent = /** @class */ (function () {
    function HomeComponent(page) {
        this.page = page;
        this.title = 'Bhavesh Sachala';
        this.data = [];
    }
    HomeComponent.prototype.ngOnInit = function () {
    };
    HomeComponent.prototype.onDrawerButtonTap = function () {
        console.log('drawe tapped');
        var sideDrawer = tns_core_modules_application__WEBPACK_IMPORTED_MODULE_2__["getRootView"]();
        sideDrawer.showDrawer();
    };
    HomeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-home',
            template: __webpack_require__("./app/home/home.component.html"),
            styles: [__webpack_require__("./app/home/home.component.css")]
        }),
        __metadata("design:paramtypes", [tns_core_modules_ui_page__WEBPACK_IMPORTED_MODULE_1__["Page"]])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./app/login/login.component.html":
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\n<ScrollView >\n    <StackLayout horizontalAlignment=\"center\" verticalAlignment=\"center\">\n            <GridLayout rows=\"33*, 67*\">\n            <GridLayout row=\"0\" class=\"logo-container\">\n                <GridLayout class=\"image-container\">\n                    <Image src=\"~/app/images/Property_Launch_Logo_png\" class = \"logoImage\"></Image>\n                </GridLayout>\n            </GridLayout>\n\n            <GridLayout row=\"1\" rows=\"*, auto\" >\n                    <GridLayout row=\"0\" rows=\"auto\" rows=\"*\">\n                            <ActivityIndicator color=\"red\" [busy]=\"true\"\n                                horizontalAlignment=\"center\" verticalAlignment=\"center\"></ActivityIndicator>\n                    </GridLayout>\n\n                    <GridLayout row=\"0\" rows=\"*\">\n\n                    <GridLayout row=\"0\" \n                                rows=\"auto, auto, auto, auto, auto\" \n                                class=\"container-padding\"\n                                verticalAlignment=\"top\">\n\n                                <GridLayout row=\"0\" rows=\"auto, auto\">\n\n                                        <TextField \n                                            #email \n                                            row=\"0\"\n                                            ios:style=\"padding:10\" \n                                            android:style=\"padding-left:10; padding-right: 10\"\n                                            hint=\"Email\" \n                                            keyboardType=\"email\"\n                                            [(ngModel)]=\"userDetails.email\" \n                                            autocorrect=\"false\"\n                                            autocapitalizationType=\"none\" \n                                            (focus)=\"onEmailFocus()\"\n                                            class=\"txtEmailLogin\"\n                                            >\n                                        </TextField>\n            \n                                        <Label \n                                        class = \"\"\n                                        row=\"1\"\n                                        #errormail \n                                        [text]=\"getEmailError()\" \n                                        \n                                        >\n                                        </Label>\n                                        \n                                </GridLayout>\n\n                                <GridLayout row=\"1\" rows=\"auto, auto\">\n\n                                        <TextField \n                                            #password \n                                            row=\"0\"\n                                            ios:style=\"padding:10\" \n                                            android:style=\"padding-left:10; padding-right: 10\"\n                                            hint=\"Password\" \n                                            keyboardType=\"email\"\n                                            [(ngModel)]=\"userDetails.password\" \n                                            autocorrect=\"false\"\n                                            autocapitalizationType=\"none\" \n                                            (focus)=\"onEmailFocus()\"\n                                            class=\"txtPasswordLogin\"\n                                            >\n                                        </TextField>\n            \n                                        <Label \n                                        *ngIf=\"hasEmailErrors()\" \n                                        class=\"eloha-font-semibold m-t-2 login-field-label color-danger\"\n                                        row=\"1\" [text]=\"getEmailError()\" \n                                        [ngClass]=\"{'font-size-md': !isTablet(), 'font-size-md-tablet': isTablet()}\"\n                                        >\n                                        </Label>\n                                        \n                                </GridLayout>\n\n                                <GridLayout row=\"2\" rows=\"auto\">\n\n                                        <Label \n                                        text = \"Forgot Password?\"\n                                        class = \"forgotPassword\"\n                                        horizontalAlignment=\"center\"\n                                        >\n                                        </Label>\n                                        \n                                </GridLayout>\n\n                                <GridLayout row=\"3\" rows=\"auto\">\n\n                                        <Button\n                                        text = \"Login\"\n                                        class = \"btnLogin\"\n                                        (tap) = \"btnLoginAction()\"\n                                        >\n                                        </Button>\n                                        \n                                </GridLayout>\n\n                                <GridLayout row=\"4\" rows=\"auto\" >\n\n                                        <Label\n                                        text = \"Don't have an account sign up here\"\n                                        class = \" lbldontac\"\n                                        horizontalAlignment=\"center\"\n                                        >\n                                        </Label>\n                                \n                                </GridLayout>\n\n                    </GridLayout>\n                    \n                    </GridLayout>\n                    \n            </GridLayout>\n        </GridLayout>\n    </StackLayout>\n</ScrollView>"

/***/ }),

/***/ "./app/login/login.component.scss":
/***/ (function(module, exports) {

module.exports = ".logo-container {\n  height: 150;\n  width: 200;\n  margin-top: 80; }\n\n.lblLoginEmail {\n  margin-top: 10;\n  margin-left: 20;\n  margin-right: 20; }\n\n.btnLogin {\n  margin-top: 12;\n  margin-left: 20;\n  margin-right: 20;\n  height: 40;\n  background-color: #1b75bb;\n  border-radius: 5;\n  color: white; }\n\n.txtEmailLogin {\n  border-width: 0.5;\n  margin-top: 40;\n  margin-left: 20;\n  margin-right: 20;\n  border-radius: 3;\n  border-color: #aaaaaa; }\n\n.txtPasswordLogin {\n  border-width: 0.5;\n  margin-top: 15;\n  margin-left: 20;\n  margin-right: 20;\n  border-radius: 3;\n  border-color: #aaaaaa; }\n\n.forgotPassword {\n  height: 20;\n  margin-top: 12;\n  margin-left: 20;\n  color: #1b75bb; }\n\n.lbldontac {\n  height: 20;\n  margin-top: 12;\n  margin-right: 20; }\n\n.btnOwner {\n  margin-top: 30;\n  height: 40;\n  width: 150;\n  margin-left: 20;\n  background-color: #1b75bb;\n  border-radius: 3;\n  color: white; }\n\n.btnAgent {\n  margin-top: 30;\n  height: 40;\n  width: 150;\n  margin-right: 5;\n  background-color: #1b75bb;\n  border-radius: 3;\n  color: white; }\n"

/***/ }),

/***/ "./app/login/login.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var _model_user_user_master__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./app/model/user/user-master.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("../node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _utilites_message__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./app/utilites/message.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var LoginComponent = /** @class */ (function () {
    function LoginComponent(route, router) {
        this.route = route;
        this.router = router;
        this.userDetails = new _model_user_user_master__WEBPACK_IMPORTED_MODULE_0__["UserMaster"]();
    }
    LoginComponent.prototype.ngOnInit = function () {
    };
    LoginComponent.prototype.isValid = function () {
        if (this.userDetails.email == null) {
            this.alert('Enter your email');
            return false;
        }
        else if (this.userDetails.password == null) {
            this.alert('Enter your password');
            return false;
        }
        return true;
    };
    LoginComponent.prototype.btnLoginAction = function () {
        // console.log(Message.APP_NAME);
        // if (this.isValid()) {
        //   this.callUserLogin();
        // }
        this.callUserLogin();
    };
    LoginComponent.prototype.getEmailError = function () {
        var email = this.erroEmail.nativeElement;
        if (this.userDetails.email == null) {
            email.text = 'Helloo email';
        }
        else {
            email.text = '';
        }
    };
    LoginComponent.prototype.callUserLogin = function () {
        this.router.navigate(['/home']);
    };
    LoginComponent.prototype.alert = function (message) {
        return alert({
            title: _utilites_message__WEBPACK_IMPORTED_MODULE_3__["Message"].APP_NAME,
            okButtonText: _utilites_message__WEBPACK_IMPORTED_MODULE_3__["Message"].OK,
            message: message
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('password'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], LoginComponent.prototype, "passwordField", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('email'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], LoginComponent.prototype, "emailField", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('errormail'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], LoginComponent.prototype, "erroEmail", void 0);
    LoginComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-login',
            template: __webpack_require__("./app/login/login.component.html"),
            styles: [__webpack_require__("./app/login/login.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./app/model/user/user-master.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserMaster", function() { return UserMaster; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Result", function() { return Result; });
var UserMaster = /** @class */ (function () {
    function UserMaster() {
    }
    return UserMaster;
}());

var Result = /** @class */ (function () {
    function Result() {
    }
    return Result;
}());



/***/ }),

/***/ "./app/utilites/message.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Message", function() { return Message; });
var Message = /** @class */ (function () {
    function Message() {
    }
    Message.BOOK_SHELF_NONE = 'None';
    Message.BOOK_SHELF_FULL = 'Full';
    Message.APP_NAME = 'Property Launch';
    Message.OK = 'OK';
    return Message;
}());



/***/ }),

/***/ "./main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var nativescript_angular_platform__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/nativescript-angular/platform.js");
/* harmony import */ var nativescript_angular_platform__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(nativescript_angular_platform__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./app/app.module.ts");

            __webpack_require__("../node_modules/nativescript-dev-webpack/load-application-css-angular.js")();
            
            
        if (false) {}
        
            
        __webpack_require__("../node_modules/tns-core-modules/bundle-entry-points.js");
        // this import should be first in order to load some required settings (like globals and reflect-metadata)


// A traditional NativeScript application starts by initializing global objects, setting up global CSS rules, creating, and navigating to the main page. 
// Angular applications need to take care of their own initialization: modules, components, directives, routes, DI providers. 
// A NativeScript Angular app needs to make both paradigms work together, so we provide a wrapper platform object, platformNativeScriptDynamic, 
// that sets up a NativeScript application and can bootstrap the Angular framework.
Object(nativescript_angular_platform__WEBPACK_IMPORTED_MODULE_0__["platformNativeScriptDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_1__["AppModule"]);

    
        
        

/***/ }),

/***/ "./package.json":
/***/ (function(module) {

module.exports = {"android":{"v8Flags":"--expose_gc"},"main":"main.js","name":"migration-ng","version":"4.1.0"};

/***/ })

/******/ });